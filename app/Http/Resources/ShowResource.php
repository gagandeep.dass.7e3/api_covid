<?php

namespace App\Http\Resources;

use App\Models\CCAAs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);

        /*
        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')
            ->first();

        $ccaa = ccaas::where('id',$this->ccaas_id)->first();
        return [
            'fecha' => $this->fecha,
            'ccaas' => $ccaa->nombre,
            'pais' => $pais->nombre,
            'media' => $this->media,

        ];
*/
        /*
        $pais=DB::table('paises')
            ->join('ccaas','paises.id','ccaas.pais_id')
            ->select('paises.*')->first();
        $ccaa=CCAAs::where('id',$this->ccaas_id)->first();
        if(isset($this->incidencia)){
            return [
                'fecha' => $this->fecha,
                'pais'=>$pais->nombre,
                'ccaas_id' =>$ccaa->nombre,
                'value' => $this->incidencia,
            ];
        }else{
            return [
                'fecha' => $this->fecha,
                'pais'=>$pais->nombre,
                'ccaas_id' =>$ccaa->nombre,
                'value' => $this->numero,
            ];
        }
        */

    }
}
