<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ia14 extends Model
{
    use HasFactory;
    protected $table = 'ia14';
    public $timestamps = false;
    protected $fillable = ['fecha', 'ccaas_id', 'incidencia'];
                                    //ccaas_id
}
