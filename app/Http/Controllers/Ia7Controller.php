<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ShowResource;

class Ia7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ia7 = new ia7();
        $ia7->fecha = $request->fecha;
        $ia7->ccaa_id = $request->ccaa_id;
        $ia7->media = $request->media;
        $ia7->save();
        return response()->json($ia7);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ShowResource
     */
    public function show($id)
    {
        $ia7 = DB::select(DB::raw("select * from ia7 where fecha='$id'"));
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$ia7],200);

    }


    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $ia7 = DB::select(DB::raw("select * from ia7 where fecha BETWEEN '$id1' and '$id2' "));

        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($ia7);

    }


    public function showAll()
    {

        $ia7 = ia7::all();
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia7],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ia7=ia7::where('id',$id)->first();
        if(!$ia7){
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'La fecha no existe'])],404);
        }else{
            $ia7->id=$id;
            $ia7->fecha = $request->fecha;
            $ia7->ccaa_id = $request->ccaa_id;
            $ia7->media = $request->media;
            $ia7->save();
            return response()->json($ia7);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ia7=ia7::destroy($id);
        if($ia7==1){
            return response()->json(['status'=>'ok','data'=>$ia7],200);
        }else{
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'ID not found / not exist'])],404);
        }
    }
}
