<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\muertos;
use App\Models\Casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MuertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $muertos = new muertos();
        $muertos->fecha = $request->fecha;
        $muertos->ccaa_id = $request->ccaa_id;
        $muertos->media = $request->media;
        $muertos->save();
        return response()->json($muertos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ShowResource
     */
    public function show($id)
    {
        $muertos = DB::select(DB::raw("select * from muertos where fecha='$id'"));
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$muertos],200);

    }

    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $muertos = DB::select(DB::raw("select * from muertos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($muertos);

    }

    public function showAll()
    {

        $muertos = muertos::all();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $muertos=muertos::where('id',$id)->first();
        if(!$muertos){
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'No existe la fecha'])],404);
        }else{
            $muertos->id=$id;
            $muertos->fecha = $request->fecha;
            $muertos->ccaa_id = $request->ccaa_id;
            $muertos->media = $request->media;
            $muertos->save();
            return response()->json($muertos);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $muertos=muertos::destroy($id);
        if($muertos==1){
            return response()->json(['status'=>'ok','data'=>$muertos],200);
        }else{
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'El ID no existe'])],404);
        }

    }
}
