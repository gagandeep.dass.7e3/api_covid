<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\casos;
use App\Models\ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $casos = new casos();
        $casos->fecha = $request->fecha;
        $casos->ccaa_id = $request->ccaa_id;
        $casos->media = $request->media;
        $casos->save();
        return response()->json($casos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ShowResource
     */
    public function show($id)
    {

        $casos = DB::select(DB::raw("select * from casos where fecha='$id'"));
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return response()->json(['status'=>'ok','data'=>$casos],200);

    }

    public function showCollection($id1,$id2)
    {

        if ($id1 > $id2 )
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'La fecha inicial es mayor'])],404);

        $casos = DB::select(DB::raw("select * from casos where fecha BETWEEN '$id1' and '$id2' "));

        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return new CovidCollection($casos);

    }

    public function showAll()
    {

        $casos = casos::all();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $casos=casos::where('id',$id)->first();
        if(!$casos){
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'No existe la fecha'])],404);
        }else{
            $casos->id=$id;
            $casos->fecha = $request->fecha;
            $casos->ccaa_id = $request->ccaa_id;
            $casos->media = $request->media;
            $casos->save();
            return response()->json($casos);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $casos=casos::destroy($id);
        if($casos==1){
            return response()->json(['status'=>'ok','data'=>$casos],200);
        }else{
            return response()->json(['errors' =>Array(['code'=>404, 'message'=>'El id no existe'])],404);
        }

    }

}
